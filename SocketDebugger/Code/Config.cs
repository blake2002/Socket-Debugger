﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SocketClient
{
    /// <summary>
    /// 
    /// </summary>
    public class Config : SelfSerializer
    {

        /// <summary>
        /// 
        /// </summary>
        public Config()
        {
        }

        #region HasLastConfigs
        public bool HasLastConfigs()
        {
            IPAddress r;
            if (LastIPAddress.Length == 0 || IPAddress.TryParse(LastIPAddress, out r) == false)
            {
                return false;
            }

            return true;
        }
        #endregion //HasLastConfigs

        #region IPAddressList
        /// <summary>
        /// 
        /// </summary>
        public List<string> IPAddressList
        {
            get
            {
                if (_ipAddressList == null)
                {
                    _ipAddressList = new List<string>();
                }
                return _ipAddressList;
            }
            set { _ipAddressList = value; }
        } private List<string> _ipAddressList;
        #endregion //IPAddressList

        #region PortList
        /// <summary>
        /// 
        /// </summary>
        public List<UInt16> PortList
        {
            get
            {
                if (_portList == null)
                {
                    _portList = new List<ushort>();
                }
                return _portList;
            }

            set { _portList = value; }
        } private List<UInt16> _portList;
        #endregion //PortList

        #region LastIPAddress
        /// <summary>
        /// 
        /// </summary>
        public string LastIPAddress
        {
            get
            {
                if (_lastIPAddress == null)
                {
                    _lastIPAddress = string.Empty;
                }
                return _lastIPAddress;
            }
            set
            {
                _lastIPAddress = value;
            }
        } private string _lastIPAddress;
        #endregion //LastIPAddress

        #region LastProtocolType
        /// <summary>
        /// 
        /// </summary>
        public ProtocolType LastProtocolType
        {
            get { return _lastProtocolType; }
            set { _lastProtocolType = value; }
        } private ProtocolType _lastProtocolType = ProtocolType.Tcp;
        #endregion //LastProtocolType

        #region LastPort
        /// <summary>
        /// 
        /// </summary>
        public UInt16 LastPort
        {
            get
            {
                return _lastPort;
            }
            set
            {
                _lastPort = value;
            }
        } private UInt16 _lastPort;
        #endregion //LastPort

        #region LogDataMode
        /// <summary>
        /// 
        /// </summary>
        public DataMode LogDataMode
        {
            get { return _logDataMode; }
            set { _logDataMode = value; }
        } private DataMode _logDataMode;
        #endregion //LogDataMode

        #region SendDataMode
        /// <summary>
        /// 
        /// </summary>
        public DataMode SendDataMode
        {
            get { return _sendDataMode; }
            set { _sendDataMode = value; }
        } private DataMode _sendDataMode;
        #endregion //SendDataMode

        #region MarkIPAddress
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iPAddress"></param>
        internal void MarkIPAddress(IPAddress ipAddress)
        {
            string ip = ipAddress.ToString();
            if (!this.IPAddressList.Contains(ip))
            {
                this.IPAddressList.Add(ip);
            }
            this.LastIPAddress = ip;
        }
        #endregion //MarkIPAddress

        #region MarkProtocolType
        internal void MarkProtocolType(ProtocolType protocolType)
        {
            this.LastProtocolType = protocolType;
        }
        #endregion //MarkProtocolType

        #region MarkPort
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        internal void MarkPort(ushort port)
        {
            if (!this.PortList.Contains(port))
            {
                this.PortList.Add(port);
            }
            this.LastPort = port;
        }
        #endregion //MarkPort

        #region ListViewColumnWidths
        /// <summary>
        /// 
        /// </summary>
        public int[] ListViewColumnWidths
        {
            get
            {
                if (_listViewColumnWidths == null)
                {
                    _listViewColumnWidths = new int[0];
                }
                return _listViewColumnWidths;
            }
            set
            {
                _listViewColumnWidths = value;
            }
        } private int[] _listViewColumnWidths;
        #endregion //ListViewColumnWidths

        #region FormWindowState
        /// <summary>
        /// 
        /// </summary>
        public FormWindowState FormWindowState
        {
            get { return _formWindowState; }
            set { _formWindowState = value; }
        } private FormWindowState _formWindowState;
        #endregion //FormWindowState

        #region FormSize
        /// <summary>
        /// 
        /// </summary>
        public Size FormSize
        {
            get
            {
                if (_formSize == null)
                {
                    _formSize = new Size();
                }
                return _formSize;
            }
            set
            {
                _formSize = value;
            }
        } private Size _formSize;
        #endregion //FormSize

        #region Location
        /// <summary>
        /// 
        /// </summary>
        public Point Location
        {
            get
            {
                if (_location == null)
                {
                    _location = new Point();
                }
                return _location;
            }
            set
            {
                _location = value;
            }
        } private Point _location;
        #endregion //Location

        #region SendCollection
        /// <summary>
        /// 
        /// </summary>
        public SendCollection SendCollection
        {
            get
            {
                if (_sendCollection == null)
                {
                    _sendCollection = new SendCollection();
                }

                return _sendCollection;
            }
            set { _sendCollection = value; }
        } private SendCollection _sendCollection;

        #endregion //SendCollection

        #region SerialPortSettings
        /// <summary>
        /// 
        /// </summary>
        public SerialPortSettings SerialPortSettings
        {
            get
            {
                if (_serialPortSettings == null)
                {
                    _serialPortSettings = SerialPortSettings.Default;
                }
                return _serialPortSettings;
            }
            set
            {
                _serialPortSettings = value;
            }
        } private SerialPortSettings _serialPortSettings;
        #endregion //SerialPortSettings

        #region LocalPort
        /// <summary>
        /// 
        /// </summary>
        public UInt16 LocalPort
        {
            get
            {
                return _localPort;
            }
            set
            {
                _localPort = value;
            }
        } private UInt16 _localPort;
        #endregion //LocalPort

        #region IsUseLocalPort
        /// <summary>
        /// 
        /// </summary>
        public bool IsUseLocalPort
        {
            get
            {
                return _isUseLocalPort;
            }
            set
            {
                _isUseLocalPort = value;
            }
        } private bool _isUseLocalPort;
        #endregion //IsUseLocalPort

        #region EnabledWarpperList
        /// <summary>
        /// 
        /// </summary>
        public List<string> EnabledWrapperList
        {
            get
            {
                if (_enabledWrapperList == null)
                {
                    _enabledWrapperList = new List<string>();
                }
                return _enabledWrapperList;
            }
            set
            {
                _enabledWrapperList = value;
            }
        } private List<string> _enabledWrapperList;
        #endregion //EnabledWarpperList

        #region EnabledReply
        /// <summary>
        /// 
        /// </summary>
        public bool EnabledReply
        {
            get
            {
                return _enabledReply;
            }
            set
            {
                _enabledReply = value;
            }
        } private bool _enabledReply = false;
        #endregion //EnabledReply

        #region IsEnabled
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeOfWrapper"></param>
        /// <returns></returns>
        public bool IsEnabled(Type typeOfWrapper)
        {
            string typeName = typeOfWrapper.FullName.Trim();
            foreach (string s in EnabledWrapperList)
            {
                if (s == typeName)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion //IsEnabled

        #region ReplyManagerColumnWidths
        /// <summary>
        /// 
        /// </summary>
        public List<int> ReplyManagerColumnWidths
        {
            get
            {
                if (_replyManagerColumnWidths == null)
                {
                    _replyManagerColumnWidths = new List<int>();
                }
                return _replyManagerColumnWidths;
            }
            set { _replyManagerColumnWidths = value; }
        } private List<int> _replyManagerColumnWidths;
        #endregion //ReplyManagerColumnWidths

        #region ReplyManagerSize
        /// <summary>
        /// 
        /// </summary>
        public Size ReplyManagerSize
        {
            get
            {
                if (_replyManagerSize == null)
                {
                    _replyManagerSize = new Size();
                }
                return _replyManagerSize;
            }
            set
            {
                _replyManagerSize = value;
            }
        } private Size _replyManagerSize;
        #endregion //ReplyManagerSize

        #region ToolBarVisible
        public bool ToolBarVisible { get; set; }
        #endregion //ToolBarVisible

        #region StatusBarVisible
        public bool StatusBarVisible { get; set; }
        #endregion //StatusBarVisible

        public bool HexBoxLineVisible { get; set; }

        public bool HexBoxColumnVisible { get; set; }
    }
}
